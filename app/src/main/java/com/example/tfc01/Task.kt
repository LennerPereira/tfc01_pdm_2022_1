package com.example.tfc01

import android.os.Parcel
import android.os.Parcelable

class Task(
    val description: String,
    var isUrgent: Boolean,
    var isDone: Boolean
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel?, flag: Int) {
        parcel!!.writeString(description)
        parcel!!.writeByte(if(isUrgent) 1.toByte() else 0.toByte())
        parcel!!.writeByte(if(isDone) 1.toByte() else 0.toByte())
    }

    companion object CREATOR : Parcelable.Creator<Task> {
        override fun createFromParcel(parcel: Parcel): Task {
            return Task(parcel)
        }

        override fun newArray(size: Int): Array<Task?> {
            return arrayOfNulls(size)
        }
    }

}
