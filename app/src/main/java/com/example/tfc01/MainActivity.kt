package com.example.tfc01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var works: ArrayList<Task>
    private lateinit var textTarefa: EditText
    private lateinit var lvWorks: RecyclerView
    private lateinit var urgency: SwitchCompat
    private lateinit var adapter: TaskAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.textTarefa = findViewById(R.id.textTarefa)
        this.lvWorks    = findViewById(R.id.lvWork)
        this.works      = ArrayList()
        this.urgency    = findViewById(R.id.urgency)
        this.adapter    = TaskAdapter(this.works)
        this.lvWorks.layoutManager = LinearLayoutManager(this)
        this.lvWorks.adapter = this.adapter
    }

    fun onClickSend(v: View) {
        val tarefa = this.textTarefa.text.toString()
        if(tarefa.trim().isNotEmpty()) {
            val isUrgent = this.urgency.isChecked
            val isDone = false
            val task = Task(tarefa,isUrgent,isDone)
            this.works.add(task)
            this.textTarefa.setText("")
            this.urgency.isChecked = false
            this.adapter.notifyItemInserted(this.works.size - 1)
        }
    }
}