package com.example.tfc01

import android.view.View
import android.widget.CheckBox
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

class TaskViewHolder(
    itemView: View
): RecyclerView.ViewHolder(itemView) {
    private val txtTask: TextView = itemView.findViewById(R.id.txtTask)
    private val flUrgent: FrameLayout = itemView.findViewById(R.id.flUrgent)
    private val cbDone: CheckBox = itemView.findViewById(R.id.cbDone)
    private lateinit var currentTask: Task

    init {
        this.cbDone.setOnCheckedChangeListener { _, done ->
            this.currentTask.isDone = done
        }
    }

    fun bind(task: Task){
        currentTask = task
        this.txtTask.text = currentTask.description
        val color = if (currentTask.isUrgent) {
             ContextCompat.getColor(itemView.context, R.color.red)
        } else {
            ContextCompat.getColor(itemView.context, R.color.green)
        }
        this.flUrgent.background.setTint(color)
        this.cbDone.isChecked = currentTask.isDone
    }
}