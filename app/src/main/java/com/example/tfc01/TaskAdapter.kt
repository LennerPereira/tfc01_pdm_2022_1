package com.example.tfc01

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class TaskAdapter(
    private val works: ArrayList<Task>
): RecyclerView.Adapter<TaskViewHolder> (){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(
            R.layout.itemview_task,
            parent,
            false
        )
        return  TaskViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {

        holder.bind(this.works[position])
    }

    override fun getItemCount(): Int {
        return works.size
    }
}